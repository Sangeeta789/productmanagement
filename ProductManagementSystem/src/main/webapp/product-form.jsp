<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Product Management</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: purple">
			<div>
				<a class="navbar-brand"><b style="color: white">Product Management</b> </a>
			</div>
			<ul class="navbar-nav">
				<li><a href="<%=request.getContextPath()%>/list"
					class="nav-link">Product Details</a></li>
			</ul>
			<div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
		  		<ul class="navbar-nav ">
					<li class="nav-item">
			  			<a href="<%=request.getContextPath()%>/loginPage" class="btn btn-outline-warning my-2 my-sm-0">Log out</a>
					</li>	
		  		</ul>		  
			</div>
		</nav>
	</header>
	<br>
	<div class="container col-md-5">
		<div class="card">
			<div class="card-body">
				<c:if test="${prod != null}">
					<form action="update" method="post">
				</c:if>
				<c:if test="${prod == null}">
					<form action="add" method="post">
				</c:if>
				<caption>
					<h2>
					<c:if test="${prod != null}">
            			Edit Product
            		</c:if>
					<c:if test="${prod == null}">
            			Add New Product
            		</c:if>
					</h2>
				</caption>
				<c:if test="${prod != null}">
					
					<fieldset class="form-group">
					<label>Product ID</label> <input type="number" 
					value="<c:out value='${prod.id}' />" class="form-control"
					name="id" readonly />
					</fieldset>
					
				</c:if>
				
				<c:if test="${prod == null}">
				<fieldset class="form-group">
					<label>Product ID</label> <input type="number" 
					value="<c:out value='${prod.id}' />" class="form-control"
					name="id" required="required" placeholder="Enter product ID"/>
				</fieldset>
				</c:if>
				
				<fieldset class="form-group">
					<label>Name</label> <input type="text"
						value="<c:out value='${prod.name}' />" class="form-control"
						name="name" required="required" placeholder="Enter product name"/>
				</fieldset>
				<fieldset class="form-group">
					<label>Description</label> <input type="text"
						value="<c:out value='${prod.desc}' />" class="form-control"
						name="desc" placeholder="Enter product description"/>
				</fieldset>
				<fieldset class="form-group">
					<label>Price</label> <input type="number"
						value="<c:out value='${prod.price}' />" class="form-control"
						name="price" placeholder="Enter product price"/>
				</fieldset>
				<fieldset class="form-group">
					<label>Quantity</label> <input type="number"
						value="<c:out value='${prod.qty}' />" class="form-control"
						name="qty" placeholder="Enter product quantity"/>
				</fieldset>
				<fieldset class="form-group">
				<label>Category</label>
				<select class="form-control" name="category" >
  					<option selected>Choose product category</option>
  					<c:forEach  items = "${catList}" var="cat" >
  						<option ><c:out value = "${cat}"/></option>
  					</c:forEach>
				</select>
				</fieldset>
				<c:if test="${prod != null}">
					<button type="submit" class="btn btn-success">Update</button>
				</c:if>
				<c:if test="${prod == null}">
					<button type="submit" class="btn btn-success">Add</button>
				</c:if>
				</form>
			</div>
		</div>
	</div>
</body>
</html>

