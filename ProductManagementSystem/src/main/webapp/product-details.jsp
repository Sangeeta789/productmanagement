<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Product Management</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: purple">
			<div>
				<a class="navbar-brand"><b style="color: white">Product Management</b></a>
			</div>
			<ul class="navbar-nav">
				<li>
				<a href="<%=request.getContextPath()%>/list" class="nav-link">Product Details</a>
				<li>
				<li>
				<a href="<%=request.getContextPath()%>/cat" class="nav-link">Shopping</a>
				<li>
				<li>
				<a href="<%=request.getContextPath()%>/orderpage" class="nav-link">My Orders</a>
				<li>
			</ul>
			<div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
		  		<ul class="navbar-nav ">
					<li class="nav-item">
			  			<a href="<%=request.getContextPath()%>/loginPage" class="btn btn-outline-warning my-2 my-sm-0">Log out</a>
					</li>	
		  		</ul>		  
			</div>
		</nav>
	</header>
	<br>
	
	<div class="row">
		<div class="container">
			<h3 class="text-center">Details of Products</h3>
			<hr>
			<div class="container text-right">
				<a href="<%=request.getContextPath()%>/new" class="btn btn-success">Add Product</a>
				&nbsp;&nbsp;&nbsp;&nbsp; 
				<a href="<%=request.getContextPath()%>/addCat" class="btn btn-success">Add Category</a>
			</div>
			<br>
			<table class="table table-bordered">
				<thead style="background-color:purple;color: white">
					<tr>
						<td>Product ID</td>
						<td>Name</td>
						<td>Description</td>
						<td>Price</td>
						<td>Quantity</td>
						<td>Category</td>
						<td>Action</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="prod" items = "${productDetails}">
						<tr>
							<td><c:out value = "${prod.id}"/></td>
							<td><c:out value = "${prod.name}"/></td>
							<td><c:out value = "${prod.desc}"/></td>
							<td><c:out value = "${prod.price}"/></td>
							<td><c:out value = "${prod.qty}"/></td>
							<td><c:out value = "${prod.category}"/></td>
							<td><a href="edit?id=<c:out value='${prod.id}'/>" class="btn btn-warning"> Edit</a>&nbsp;&nbsp;&nbsp;&nbsp; <a href="delete?id=<c:out value='${prod.id}'/>" class="btn btn-danger">Delete</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	
</body>

</html>