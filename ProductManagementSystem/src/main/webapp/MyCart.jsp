<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Product Management</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: purple">
			<div>
				<a class="navbar-brand"><b style="color: white">ShopKart</b></a>
			</div>
			<ul class="navbar-nav">
				<li>
				<a href="<%=request.getContextPath()%>/cat" class="nav-link">Shopping</a>
				<li>
				<li>
				<a href="<%=request.getContextPath()%>/orderpage" class="nav-link">My Orders</a>
				<li>
			</ul>
			<div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
		  		<ul class="navbar-nav ">
					<li class="nav-item">
			  			<a href="<%=request.getContextPath()%>/loginPage" class="btn btn-outline-warning my-2 my-sm-0">Log out</a>
					</li>	
		  		</ul>		  
			</div>
		</nav>
	</header>
	<br>
	<div class="row">
		<div class="container">
			<h3 class="text-center">Add To Cart</h3>
			<hr>
			<div class="container" style="width:500px">
				<form class="form-group" action="myorder" method="POST">
					<label><b>Name</b></label> <input type="text"
						value="<c:out value='${prodName}' />" class="form-control m-2 "
						name="prodName" readonly/>
					<label><b>Price</b></label> <input type="number"
						value="<c:out value='${prodPrice}' />" class="form-control m-2 "
						name="prodPrice" readonly/>
					<label><b>Quantity</b></label> <input type="number"
						 class="form-control m-2"
						name="prodQty" placeholder="Enter product quantity"/>
					<input class="btn btn-success m-2" type="submit" name="submit" value="Add to cart"/>
				</form>
			</div>
		</div>
	</div>
</body>
</html>