<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Product Management</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: purple">
			<div>
				<a class="navbar-brand"><b style="color: white">ShopKart</b></a>
			</div>
			<ul class="navbar-nav">
				<li>
				<a href="<%=request.getContextPath()%>/cat" class="nav-link">Shopping</a>
				<li>
				<li>
				<a href="<%=request.getContextPath()%>/orderpage" class="nav-link">My Orders</a>
				<li>
			</ul>
			<div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
		  		<ul class="navbar-nav ">
					<li class="nav-item">
			  			<a href="<%=request.getContextPath()%>/loginPage" class="btn btn-outline-warning my-2 my-sm-0">Log out</a>
					</li>	
		  		</ul>		  
			</div>
		</nav>
	</header>
	<br>
	<div class="row">
		<div class="container">
			<h3 class="text-center">Add Category</h3>
			<hr>
			<div class="container" style="width:500px">
			<form class="form-group"  action="addCatImg" method="post" enctype="multipart/form-data">
			<label for="category-name">Name:</label>
			 <input type="text" id="category-name" name="category-name" class="form-control m-2" >
			
			<label for="category-image">Image:</label> 
			<input type="file" id="category-image" name="category-image" class="form-control m-2" >
			
			<input type="submit" value="UPLOAD" class="btn btn-success m-2"> </form>
			</div>
		</div>
	</div>
</body>
</html>