package com.prasad.productmanagement.bean;

import javax.servlet.http.Part;

public class Category {
	String category_name;
	Part category_image;
	
	public Category() {
		super();
	}
	
	public Category(String category_name, Part category_image) {
		super();
		this.category_name = category_name;
		this.category_image = category_image;
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public Part getCategory_image() {
		return category_image;
	}

	public void setCategory_image(Part category_image) {
		this.category_image = category_image;
	}
	
	
	
}
