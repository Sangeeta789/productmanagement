package com.prasad.productmanagement.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.prasad.productmanagement.bean.MyOrders;
import com.prasad.productmanagement.bean.Product;
import com.prasad.productmanagement.bean.UserData;
import com.prasad.productmanagement.dao.CategoryDao;
import com.prasad.productmanagement.dao.ProductDao;
import com.prasad.productmanagement.bean.UserData;
import com.prasad.productmanagement.dao.UserDao;

/**
 * Servlet implementation class ProductServlet
 */
@WebServlet("/")
@MultipartConfig(
		fileSizeThreshold = 1024 * 1024,
		maxFileSize = 1024 * 1024 * 10,
		maxRequestSize = 1024 * 1024 * 11
		
		)
public class ProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private ProductDao prodDao;
	
	
	private Gson gson = new Gson();
	
       
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init() throws ServletException {
		prodDao = new ProductDao();
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		doGet(request, response);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String action = request.getServletPath();
		
		switch(action) {
		
		case "/new":
			showNewForm(request,response);
			break;
		
		case "/add":
			try {
				addProduct(request,response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		
		case "/delete":
			try {
				deleteProduct(request,response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			break;
			
		case "/edit":
			try {
				showEditProduct(request,response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
			
		case "/update":
			try {
				editProduct(request,response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			break;
			
		case "/shop":
			try {
				shopPage(request,response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
			
		case "/cat":
			try {
				selectCategory(request,response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
			
		case "/mycart":
			try {
				addToCart(request,response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
			
		case "/myorder":
			try {
				myOrder(request,response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
			
		case "/register":
			try {
				addUser(request,response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
			
		case "/loginPage":
				showLogin(request,response);
			break;
			
		case "/login":
			try {
				login(request,response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
			
		case "/showRegister":
			showRegistration(request,response);
			break;
			
		case "/orderpage":
			try {
				showOrderPage(request,response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		
		case "/deleteOrder":
			try {
				deleteOrder(request,response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
			
		case "/addCat":
			
				showCategoryPage(request,response);
			
			break;	
			
		case "/addCatImg":
			
			try {
				
				addCategoryImage(request,response);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			break;	
			
		
			
		default:
			try {
				productDetails(request,response);
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}	
	}
	
	

	private void showNewForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		CategoryDao catDao = new CategoryDao();
		List<String> catList = new ArrayList<>();
		catList = catDao.selectCatFromCat();
		request.setAttribute("catList", catList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("product-form.jsp");
		dispatcher.forward(request, response);
		
	}
	
	private void addProduct(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String desc = request.getParameter("desc");
		double price = Double.parseDouble(request.getParameter("price"));
		int qty = Integer.parseInt(request.getParameter("qty"));
		String category = request.getParameter("category");
		Product newProd = new Product(id,name,desc,price,qty,category);
		
		String prodJSON = this.gson.toJson(newProd);
		prodDao.addProduct(prodJSON);
		response.sendRedirect("list");
		
	}
	
	private void deleteProduct(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		try {
			prodDao.deleteProduct(id);
		}catch(Exception e){
			e.printStackTrace();
		}
		response.sendRedirect("list");
	}

	private void showEditProduct(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		Product existingProduct;
		String prodString;
		try {
			prodString = prodDao.showProductById(id);
			
			
			//MARK: for json o/p
//			PrintWriter out = response.getWriter();
//			response.setContentType("application/json");
//			response.setCharacterEncoding("UTF-8");
//			out.print(prodString);
//			out.flush();
			//end
			
			existingProduct = gson.fromJson(prodString, Product.class);
			RequestDispatcher dispatcher = request.getRequestDispatcher("product-form.jsp");
			request.setAttribute("prod", existingProduct);
			dispatcher.forward(request, response);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void editProduct(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String desc = request.getParameter("desc");
		double price = Double.parseDouble(request.getParameter("price"));
		int qty = Integer.parseInt(request.getParameter("qty"));
		String category = request.getParameter("category");
		
		Product newProd = new Product(id,name,desc,price,qty,category);
		String prodJSON = this.gson.toJson(newProd);
		prodDao.editProduct(prodJSON);
		response.sendRedirect("list");
	}
	
	private void productDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException, IOException {
		try{
			String prodDetailsJSON = prodDao.showProducts();
			
			//MARK: For json o/p
//			PrintWriter out = response.getWriter();
//			response.setContentType("application/json");
//			response.setCharacterEncoding("UTF-8");
//			out.print(prodDetailsJSON);
//			out.flush();
			//end
			
			Type typeToken = new TypeToken<List<Product>>() {}.getType();
			List<Product> prodDetails = gson.fromJson(prodDetailsJSON, typeToken);
			request.setAttribute("productDetails", prodDetails);
			RequestDispatcher dispatcher = request.getRequestDispatcher("product-details.jsp");
			dispatcher.forward(request, response);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void shopPage(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException, IOException {
		String category = request.getParameter("category");
		//System.out.println(category);
		String prodString;
		try {
			prodString = prodDao.showProductByCategory(category);
			
			
			//MARK: for json o/p
//			PrintWriter out = response.getWriter();
//			response.setContentType("application/json");
//			response.setCharacterEncoding("UTF-8");
//			out.print(prodString);
//			out.flush();
			//end
			
			Type typeToken = new TypeToken<List<Product>>() {}.getType();
			List<Product> prodDetails = gson.fromJson(prodString, typeToken);
			request.setAttribute("products", prodDetails);
			RequestDispatcher dispatcher = request.getRequestDispatcher("UserPage.jsp");
			dispatcher.forward(request, response);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void selectCategory(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException, IOException {
		try{
		String prodDetailsJSON = prodDao.showCategory();
		
		//MARK: For json o/p
//		PrintWriter out = response.getWriter();
//		response.setContentType("application/json");
//		response.setCharacterEncoding("UTF-8");
//		out.print(prodDetailsJSON);
//		out.flush();
		//end
		
		Type typeToken = new TypeToken<List<Product>>() {}.getType();
		List<Product> prodDetails = gson.fromJson(prodDetailsJSON, typeToken);
		request.setAttribute("prodCat", prodDetails);
		RequestDispatcher dispatcher = request.getRequestDispatcher("UserPage.jsp");
		dispatcher.forward(request, response);
	}catch(Exception e){
		e.printStackTrace();
	}
		
	}
	
	private void addToCart(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException, IOException {
		String prodName = request.getParameter("name");
		double prodPrice = Double.parseDouble(request.getParameter("price")); 
		request.setAttribute("prodName", prodName);
		request.setAttribute("prodPrice", prodPrice);
		RequestDispatcher dispatcher = request.getRequestDispatcher("MyCart.jsp");
		dispatcher.forward(request, response);
	}
	
	private void myOrder(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException, IOException {
		String prodName = request.getParameter("prodName");
		double prodPrice = Double.parseDouble(request.getParameter("prodPrice")); 
		int prodQty = Integer.parseInt(request.getParameter("prodQty")); 
		String ssUID = UserData.getUser_id();
		//System.out.println(ssUID);
		UserDao myOrders = new UserDao();
		myOrders.addOrderDeatails(ssUID, prodName, prodPrice, prodQty);		
		//System.out.println(prodName +" "+prodPrice+" "+prodQty+""+ ssUID);
		response.sendRedirect("orderpage");
	}
	
	private void showRegistration(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("Registration.jsp");
		dispatcher.forward(request, response);
		
	}
	
	private void showLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
		dispatcher.forward(request, response);
		
	}
	
	private void addUser(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		
		String uId =  request.getParameter("uId");
		String fname = request.getParameter("fname");
		String lname = request.getParameter("lname");
		String add = request.getParameter("add");
		long phone = Long.parseLong(request.getParameter("phone"));
		String pwd = request.getParameter("pwd");
		
		Encoder encoder = Base64.getEncoder();
		String encodepswd = encoder.encodeToString(pwd.getBytes());
		
		UserData newUser = new UserData(uId,fname,lname,add,phone,encodepswd);
		
		String userJSON = this.gson.toJson(newUser);
		System.out.println(userJSON);
		UserDao ud1 = new UserDao(userJSON);
		ud1.addUser();
		response.sendRedirect("loginPage");
		
	}
	
	private void login(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		String uId = request.getParameter("uId");
		String pwd = request.getParameter("pwd");
		Encoder encoder = Base64.getEncoder();
		String encodepswd = encoder.encodeToString(pwd.getBytes());
		
		 UserDao userDao = new UserDao();
		 UserData uddd = new UserData(uId);
		//-----------------------------------------------> 
		  
		if(userDao.validateLogin(uId,encodepswd)) {
			try {
				request.setAttribute("uId", uId);;
				RequestDispatcher dispatcher = request.getRequestDispatcher("UserPage.jsp");
				dispatcher.forward(request, response);
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else {
			response.sendRedirect("loginPage");
		}
			 
	}
	
	private void showOrderPage(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		UserDao userDao = new UserDao();
		try{
			String orderJSON = userDao.orderDetailsList();
			
			//MARK: For json o/p
//			PrintWriter out = response.getWriter();
//			response.setContentType("application/json");
//			response.setCharacterEncoding("UTF-8");
//			out.print(orderJSON);
//			out.flush();
			//end
			
			Type typeToken = new TypeToken<List<MyOrders>>() {}.getType();
			List<MyOrders> orderDetails = gson.fromJson(orderJSON, typeToken);
			request.setAttribute("orderDetails", orderDetails);
			RequestDispatcher dispatcher = request.getRequestDispatcher("MyOrder.jsp");
			dispatcher.forward(request, response);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	private void deleteOrder(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		UserDao userDao = new UserDao();
		String prod_name = request.getParameter("prod_name");
		try {
			userDao.deleteOrder(prod_name);
		}catch(Exception e){
			e.printStackTrace();
		}
		response.sendRedirect("orderpage");
	}
	
	
	
	private void showCategoryPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("AddCategory.jsp");
		dispatcher.forward(request, response);
		
	}
	
	private void addCategoryImage(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		String cat_name = request.getParameter("category-name");
		Part cat_img = request.getPart("category-image");	
		CategoryDao catDao = new CategoryDao();
		catDao.addCategory(cat_name, cat_img);
		catDao.addc_name(cat_name);
		response.sendRedirect("list");
		
	}
	
}
